#include <stdio.h>
#include <gmp.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, const char *argv[]){
	//Open input file and initialize parameters
	/*FILE *fp;
	char buffer[BUFSIZ];
	char delim[] = ",";
	fp = fopen("./input", "r");
	while(fgets(buffer, BUFSIZ,fp)){
		int size = strlen(buffer);
		char *ptr = strtok(buffer,delim);
		while(ptr != NULL){
			printf("[%s]\n", ptr);
			ptr = strtok(NULL, delim);
		}
	}
	fclose(fp);*/
	if(argc != 2){
		printf("Needs ./rsa input-string.\n");
	}

	mpz_t m, c_prime, d_prime, p_prime, q_prime;
	mpz_t c, e, d, n, m_prime;
	mpz_inits(m, c_prime, d_prime, p_prime, q_prime, c, e, d, n, m_prime,NULL);
	gmp_sscanf(argv[1], "%Zd,%Zd,%Zd,%Zd,%Zd", m, c_prime, d_prime, p_prime, q_prime);

	mpz_t p, q, phi, gcd, n_prime;
	mpz_inits(p, q, phi, gcd, n_prime, NULL);

	//Randomly choose two distinct prime integers for p and q that are 500b
	gmp_randstate_t state;
	gmp_randinit_default(state);

	while(1){
		mpz_urandomb(p, state, 1000);
		mpz_urandomb(q, state, 1000);

		mpz_nextprime(p, p);
		mpz_nextprime(q, q);

		if(mpz_sizeinbase(p,2) > 500 && mpz_sizeinbase(q,2) > 500 && mpz_cmp(p,q) != 0){
			break;
		}
	}
	//compute n
	mpz_mul(n, p, q);
	//compute phi
	mpz_sub_ui(p,p,1);
	mpz_sub_ui(q,q,1);
	mpz_mul(phi,p,q);
	
	//randomly choose e
	while(1){
		mpz_urandomm(e, state, phi);
		mpz_gcd(gcd, e, phi);
		if(mpz_cmp_d(gcd,1) == 0){
			break;
		}
	}

	//calculate d the private key
	mpz_invert(d,e,phi);

	//encrypt m and generate ciphertext c
	mpz_powm(c, m, e, n);

	
	//use p',q' to decrypt c' to get m'
	mpz_mul(n_prime, p_prime, q_prime);
	mpz_powm(m_prime, c_prime, d_prime, n_prime);

	//output data
	FILE *fp;
	fp = fopen("./output", "w+");
	gmp_fprintf(fp,"%Zd,%Zd,%Zd,%Zd,%Zd", c, e, d, n, m_prime); 
	fclose(fp);

	//free memory
	//mpz_clears(m, c_prime, d_prime, p_prime, q_prime, c, e, d, n, m_prime);
}
